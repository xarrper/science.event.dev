1. Выполнить команду в терминале : composer install
2. Создать файл в корне .env на основе .env.example
3. В файле .env установить переменные: DB_DATABASE, DB_USERNAME, DB_PASSWORD
4. Назначить папкам в корне права 777 (рекурсивно) : bootstrap, public, storage
5. Выполнить команду в терминале : php artisan key:generate
6. Выполнить команду в терминале : php artisan migrate
7. Выполнить команду в терминале : php artisan db:seed

Пользователя admin создавать не нужно, он уже существует в бд
admin@gmail.com: 12345678
