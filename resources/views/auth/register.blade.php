@extends('layouts.app')

@section('content')
    <br>
    <h2 id="page-register" class="blog-post-title">Регистрация</h2>
    <br>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="cst-container-form">
            <div class="row">
                <div class="col-md-12">
                    <p>Основные данные:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                           placeholder="Эл. почта" value="{{ old('email') }}" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="col-md-4">
                    <input type="password" name="password" id="inputPassword" autocomplete="password"
                           class="form-control @error('password') is-invalid @enderror" placeholder="Пароль">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <input type="password" name="password_confirmation" autocomplete="password_confirmation"
                           id="inputPassword2" class="form-control" placeholder="Повторить пароль">
                </div>
            </div>
        </div>
        <div class="cst-container-form">
            <div class="row">
                <div class="col-md-12">
                    <p>Данные о пользователе:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror"
                           value="{{ old('last_name') }}" placeholder="Фамилия">
                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror"
                           value="{{ old('first_name') }}" placeholder="Имя">
                    @error('first_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <input type="text" name="middle_name"
                           class="form-control @error('middle_name') is-invalid @enderror"
                           value="{{ old('middle_name') }}" placeholder="Отчество">
                    @error('middle_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <select id="select-post" class="form-control @error('post_id') is-invalid @enderror" name="post_id">
                        <option disabled selected style="display: block !important;">Выберете должность</option>
                        @foreach($posts as $post)
                            @if(old('post_id') == $post->id)
                                <option value="{{$post->id}}" data-role="{{$post->role_id}}" selected>{{$post->title}}</option>
                            @else
                                <option value="{{$post->id}}" data-role="{{$post->role_id}}">{{$post->title}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('post_id')
                    <span class="invalid-feedback" role="alert" style="display: block !important;">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4 text-left">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="role_id" id="gridRadios1" value="4" checked>
                        <label class="form-check-label" for="gridRadios1">
                            Студент
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="role_id" id="gridRadios2" value="3">
                        <label class="form-check-label" for="gridRadios2">
                            Сотрудник института
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="role_id" id="gridRadios3" value="2">
                        <label class="form-check-label" for="gridRadios3">
                            Специалист
                        </label>
                    </div>
                </div>
                <div class="col-md-4 field-user">
                    <input type="text" name="degree" class="form-control" placeholder="Степень">
                </div>
            </div>
            <br>
            <div class="row field-user">
                <div class="col-md-4">
                    <input type="text" name="group" class="form-control" placeholder="Группа">
                </div>
                <div class="col-md-4">
                    <input type="text" name="phone" class="form-control" placeholder="Телефон">
                </div>
                <div class="col-md-4">
                    <input type="text" name="birth_date" class="form-control" placeholder="Дата рождения">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary">
                    Зарегистрироваться
                </button>
            </div>
            <div class="col-md-4"></div>
        </div>
        <br>
    </form>
    @push('scripts')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @endpush
@endsection
