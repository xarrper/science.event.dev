@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col-md-8 blog-main">

                <form method="POST" class="form-signin" action="{{ route('login') }}">
                    @csrf
                    <h1 class="h3 mb-3 font-weight-normal">Вход</h1>
                    <label for="inputLogin" class="sr-only">Логин</label>
                    <input type="email" id="inputLogin" class="form-control @error('email') is-invalid @enderror" placeholder="E-mail" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <br>
                    <label for="inputPassword" class="sr-only">Пароль</label>
                    <input type="password" id="inputPassword" placeholder="Пароль" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <br>
                    <button type="submit" class="btn btn-primary">
                        Войти
                    </button>
                    <br>
                    <hr>
                    <a href="{{ route('register') }}">Зарегистрироваться</a>
                </form>
                @if($errors->any())
                    <h4>{{$errors->first()}}</h4>
                @endif
            </div>

            <aside class="col-md-4 blog-sidebar">
                <div class="p-3 mb-3 bg-light rounded">
                    <p class="mb-0">
                    <p class="cst-head">
                        <b>Отдел франдрайзинга и научно-аналитической деятельности</b>
                    </p>
                    <br>
                    <b>Контактная информация</b>
                    <p><em>Адрес:</em> ул. Татищева, 20а, литер А, учебный корпус №1 (корпус Т), каб. № 119ф.</p>
                    <p><em>Телефон:</em> 8(8512)24-64-48, 8(8512)24-64-51.</p>
                    <p><em>E-mail:</em> fundrising@asu.edu.ru</p>
                    <br>
                    <b>График работы</b>
                    <p>понедельник-пятница:</p>
                    <p>9.00-17.30</p>
                    <p>обед: 12.30-13.00</p>
                    </p>

                </div>

            </aside><!-- /.blog-sidebar -->

        </div><!-- /.row -->

@endsection
