@extends('layouts.app')

@section('navigation')
    <a class="block" href="{{ route('spec.cabinet.measure') }}"><i class="fas fa-arrow-left"></i> Назад</a>
@endsection

@section('content')
    <br>
    <h2 class="blog-post-title" id="measures">Создать меропреятие</h2>
    <br>
    <form id="profile" method="POST" action="{{ route('spec.save.measures') }}">
        @csrf

        <div class="cst-container-form">
            <div class="row">
                <div class="col-md-4">
                    <label>Название мероприятия</label>
                    <input type="text" name="text" class="form-control @error('text') is-invalid @enderror"
                           placeholder="Введите название мероприятия" value="{{ old('text') }}" autofocus>
                    @error('text')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Условие</label>
                    <input type="text" name="condition" class="form-control @error('condition') is-invalid @enderror"
                           placeholder="Введите условие мероприятия" value="{{ old('condition') }}" autofocus>
                    @error('condition')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Тип мероприятия</label>
                    <select id="select-post" class="form-control @error('type_id') is-invalid @enderror" name="type_id">
                        <option disabled selected style="display: block !important;">Выберете тип</option>
                        @foreach($types as $type)
                            @if(old('type_id') == $type->id)
                                <option value="{{$type->id}}" selected>{{$type->title}}</option>
                            @else
                                <option value="{{$type->id}}">{{$type->title}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('type_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <label>Ссылка на мероприятие</label>
                    <input type="text" name="link" class="form-control @error('link') is-invalid @enderror"
                           placeholder="Введите ссылку" value="{{ old('link') }}" autofocus>
                    @error('link')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Начало мероприятия</label>
                    <input type="text" name="start_date" class="form-control @error('start_date') is-invalid @enderror"
                           placeholder="Начало" value="{{ old('start_date') }}" autocomplete="off" autofocus>
                    @error('start_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Конец мероприятия</label>
                    <input type="text" name="end_date" class="form-control @error('end_date') is-invalid @enderror"
                           placeholder="Конец" value="{{ old('end_date') }}" autocomplete="off" autofocus>
                    @error('end_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary">
                    Создать
                </button>
            </div>
            <div class="col-md-4"></div>
        </div>
        <br>
    </form>
    @push('scripts')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @endpush
@endsection
