@extends('layouts.app')

@section('navigation')
    <a class="block" href="{{ route('spec.cabinet') }}"><i class="fas fa-arrow-left"></i> Назад</a>
@endsection

@section('content')
    <br>
    <h4>Список пользователей</h4>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">ФИО</th>
            <th scope="col">E-mail</th>
            <th scope="col">Телефон</th>
            <th scope="col">Дата рождения</th>
            <th scope="col">Должность</th>
            <th scope="col">Степень</th>
            <th scope="col">Группа</th>
            <th scope="col">Роль</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $loop->index + 1 }}</th>
                    @if($user->profile)
                        <td>{{$user->profile->last_name}} {{$user->profile->first_name}} {{$user->profile->middle_name}}</td>
                    @else
                        <td>-</td>
                    @endif
                    <td>{{$user->email}}</td>
                    <td>{{$user->profile->phone}}</td>
                    <td>{{$user->profile->birth_date}}</td>
                    <td>{{$user->profile->post->title}}</td>
                    <td>{{$user->profile->degree}}</td>
                    <td>{{$user->profile->group}}</td>
                    <td>
                        @if($user->role->title == 'student')
                        студент
                        @elseif($user->role->title == 'teacher')
                        преподаватель
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
