@extends('layouts.app')

@section('navigation')
    <a class="block" href="{{ route('spec.cabinet') }}"><i class="fas fa-arrow-left"></i> Назад</a>
@endsection

@section('content')
    <br>
    <h4>Список источников</h4>
    <br>
    <div class="row">
        <form method="POST" class="cst-square" action="{{ route('spec.save.sources') }}">
            @csrf
            <div class="col-md-5">
                <input type="text" name="url" class="form-control @error('url') is-invalid @enderror"
                       placeholder="Введите url сайта" value="{{ old('url') }}" autofocus>
                @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="col-md-5">
                <input type="text" name="function"
                       class="form-control @error('function') is-invalid @enderror" value="{{ old('function') }}" placeholder="Введите название функции для парсинга">
                @error('function')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </form>
    </div>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">URL</th>
            <th scope="col">Функция</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($sources as $source)
            <tr>
                <th scope="row">
                    {{ $loop->index + 1 }}
                </th>
                <th scope="row">
                    {{ $source->url }}
                </th>
                <th scope="row">
                    {{ $source->function }}
                </th>
                <th scope="row">
                    <form method="POST" action="{{ route('spec.remove.sources', $source) }}">
                        @csrf
                        <button type="submit" class="btn btn-primary">Удалить</button>
                    </form>
                </th>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
