@extends('layouts.app')

@section('content')
    <br>
    <div class="row mb-2">
        <a class="blog-header-logo text-dark cst-square" href="{{route('spec.cabinet.measure')}}">
            <div class="col-md-4 cst-col-1">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Список актуальных научных мероприятий</h4>
                    </div>
                </div>
            </div>
        </a>
        <a class="blog-header-logo text-dark cst-square" href="{{route('spec.cabinet.users')}}">
            <div class="col-md-4 cst-col-2">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Пользователи АС</h4>
                    </div>
                </div>
            </div>
        </a>
        <a class="blog-header-logo text-dark cst-square" href="{{route('spec.profile')}}">
            <div class="col-md-4 cst-col-3">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Личный кабинет</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="row mb-2">
        <a class="blog-header-logo text-dark cst-square" href="{{route('spec.sources')}}">
            <div class="col-md-4 cst-col-4">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Источники</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
@endsection
