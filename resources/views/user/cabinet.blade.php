@extends('layouts.app')

@section('content')
    <br>
    <div class="row mb-2">
        <a class="blog-header-logo text-dark cst-square" href="{{ route('user.cabinet.measure') }}">
            <div class="col-md-4 cst-col-1">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Список актуальных научных мероприятий</h4>
                    </div>
                </div>
            </div>
        </a>
        <a class="blog-header-logo text-dark cst-square" href="{{ route('user.cabinet.measure.favorite') }}">
            <div class="col-md-4 cst-col-2">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Избранные научные мероприятия</h4>
                    </div>
                </div>
            </div>
        </a>
        <a class="blog-header-logo text-dark cst-square" href="{{ route('user.cabinet.measure.offer') }}">
            <div class="col-md-4 cst-col-2">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Рекомендованные научные мероприятия</h4>
                    </div>
                </div>
            </div>
        </a>
        <a class="blog-header-logo text-dark cst-square" href="{{ route('user.profile') }}">
            <div class="col-md-4 cst-col-3">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h4 class="mb-0">Личный кабинет</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
@endsection
