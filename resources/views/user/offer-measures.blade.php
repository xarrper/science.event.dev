@extends('layouts.app')

@section('navigation')
    <a class="block" href="{{ route('user.cabinet') }}"><i class="fas fa-arrow-left"></i> Назад</a>
@endsection

@section('content')
    <div class="cst-container-div">
        <h4>Мероприятия</h4>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Тип</th>
            <th scope="col">Ссылка</th>
            <th scope="col">Условие</th>
            <th scope="col">Дата</th>
            <th scope="col">Добавить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($measures as $measure)
            <tr>
                <td scope="row">
                    {{ $loop->index + 1 }}
                </td>
                <td scope="row">
                    {{ $measure->text }}
                </td>
                <td scope="row">
                    @if($measure->type)
                        {{ $measure->type->title }}
                    @else
                        -
                    @endif
                </td>
                <td scope="row">
                    <a href="{{ $measure->link }}" target="_blank">Перейти</a>
                </td>
                <td scope="row">
                    {{ $measure->condition }}
                </td>
                <td scope="row">
                    {{ $measure->start_date }} - {{ $measure->end_date }}
                </td>
                <td>
                    <form method="POST" class="cst-form" action="{{ route('user.toggle.favorite', $measure) }}">
                        @csrf
                        @if($measure->favorite())
                            <button type="submit" class="btn btn-primary cst-star"><i class="fas fa-star fa-2x"></i></button>
                        @else
                            <button type="submit" class="btn btn-primary cst-star"><i class="far fa-star fa-2x"></i></button>
                        @endif
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
