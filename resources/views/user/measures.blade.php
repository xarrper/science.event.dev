@extends('layouts.app')

@section('navigation')
    <a class="block" href="{{ route('user.cabinet') }}"><i class="fas fa-arrow-left"></i> Назад</a>
@endsection

@section('content')
    <div class="cst-container-div" id="measures">
        <h4>Мероприятия</h4>
    </div>
    <div class="row">
        <div class="col-md-12 text-right cst-container-filter">
            <div class="row">
                <form class="cst-square" action="{{ route('user.cabinet.measure') }}" >
                    <div class="col-md-3">
                        <select id="select-post" class="form-control" name="type_id">
                            <option {{ !request()->type_id ? 'selected' : '' }} value="">Выберете тип</option>
                            @foreach($types as $type)
                                <option value="{{$type->id}}" {{ request()->type_id == $type->id ? 'selected' : '' }}>{{$type->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <input id="start_date_filter" type="text" name="start_date" class="form-control"
                               placeholder="Начало" value="{{ request()->start_date }}" autocomplete="off">
                    </div>
                    <div class="col-md-3">
                        <input id="end_date_filter" type="text" name="end_date" class="form-control"
                               placeholder="Конец" value="{{ request()->end_date }}" autocomplete="off">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Применить</button>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-primary" onclick="resetForm()"><i class="fas fa-times"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Тип</th>
            <th scope="col">Ссылка</th>
            <th scope="col">Условие</th>
            <th scope="col">Дата</th>
            <th scope="col">Добавить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($measures as $measure)
            <tr>
                <td scope="row">
                    {{ $loop->index + 1 }}
                </td>
                <td scope="row">
                    {{ $measure->text }}
                </td>
                <td scope="row">
                    @if($measure->type)
                        {{ $measure->type->title }}
                    @else
                        -
                    @endif
                </td>
                <td scope="row">
                    <a href="{{ $measure->link }}" target="_blank">Перейти</a>
                </td>
                <td scope="row">
                    {{ $measure->condition }}
                </td>
                <td scope="row">
                    {{ $measure->start_date }} - {{ $measure->end_date }}
                </td>
                <td>
                    <form method="POST" class="cst-form" action="{{ route('user.toggle.favorite', $measure) }}">
                        @csrf
                        @if($measure->favorite())
                            <button type="submit" class="btn btn-primary cst-star"><i class="fas fa-star fa-2x"></i></button>
                        @else
                            <button type="submit" class="btn btn-primary cst-star"><i class="far fa-star fa-2x"></i></button>
                        @endif
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @push('scripts')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @endpush
    <script>
        function clearFilter() {
            let nameFilter = ['#select-post', '#start_date_filter', '#end_date_filter'];
            nameFilter.forEach((item) => {
                $(item).val('');
            });
        }

        function removeParamFromUrl() {
            window.history.pushState({}, document.title, "/spec/cabinet/measure");
        }

        function resetForm() {
            removeParamFromUrl();
            clearFilter();
        }
    </script>
@endsection
