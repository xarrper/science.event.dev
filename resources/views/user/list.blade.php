@extends('layouts.app')

@section('content')
    <br>
    <h4>Список пользователей</h4>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">ФИО</th>
            <th scope="col">E-mail</th>
            <th scope="col">Статус</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $loop->index + 1 }}</th>
                    @if($user->profile)
                        <td>{{$user->profile->last_name}} {{$user->profile->first_name}} {{$user->profile->middle_name}}</td>
                    @else
                        <td>-</td>
                    @endif
                    <td>{{$user->email}}</td>
                    <td>
                        <form method="POST" action="{{ route('user.active', $user) }}">
                            @csrf
                            @if($user->confirmed)
                                <button type="submit" class="btn btn-primary">Заблокировать</button>
                            @else
                                <button type="submit" class="btn btn-primary">Активировть</button>
                            @endif
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
