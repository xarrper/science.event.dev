@extends('layouts.app')

@section('navigation')
    @if($user->hasRole('spec'))
        <a class="block" href="{{ route('spec.cabinet') }}"><i class="fas fa-arrow-left"></i> Назад</a>
    @else
        <a class="block" href="{{ route('user.cabinet') }}"><i class="fas fa-arrow-left"></i> Назад</a>
    @endif
@endsection

@section('content')
    <br>
    <h2 class="blog-post-title">Личный кабинет</h2>
    <br>
{{--    <div class="text-right">--}}
{{--        <a href="#">Смена пароля</a>--}}
{{--    </div>--}}
    <form id="profile" method="POST" action="{{ route('spec.profile.update', $user->id) }}">
        @csrf

        <div class="cst-container-form">
            <div class="row">
                <div class="col-md-4">
                    <label>Фамилия</label>
                    <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror"
                           value="{{$user->profile->last_name}}" placeholder="Фамилия">
                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Имя</label>
                    <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror"
                           value="{{$user->profile->first_name}}" placeholder="Имя">
                    @error('first_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Отчество</label>
                    <input type="text" name="middle_name"
                           class="form-control @error('middle_name') is-invalid @enderror"
                           value="{{$user->profile->middle_name}}" placeholder="Отчество">
                    @error('middle_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <label>Эл. почта</label>
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                           placeholder="Эл. почта" value="{{$user->email}}" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label>Должность</label>
                    <select id="select-post" class="form-control @error('post_id') is-invalid @enderror" name="post_id">
                        @foreach($posts as $post)
                            @if($user->profile->post->id== $post->id)
                                <option value="{{$post->id}}" data-role="{{$post->role_id}}" selected>{{$post->title}}</option>
                            @else
                                <option value="{{$post->id}}" data-role="{{$post->role_id}}">{{$post->title}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('post_id')
                    <span class="invalid-feedback" role="alert" style="display: block !important;">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                @if(!$user->hasRole('spec'))
                <div class="col-md-4 field-user">
                    <label>Степень</label>
                    <input type="text" name="degree" value="{{$user->profile->degree}}" class="form-control" placeholder="Степень">
                </div>
                @endif
            </div>
            @if(!$user->hasRole('spec'))
            <br>
            <div class="row field-user">
                <div class="col-md-4">
                    <label>Группа</label>
                    <input type="text" name="group" value="{{$user->profile->group}}" class="form-control" placeholder="Группа">
                </div>
                <div class="col-md-4">
                    <label>Телефон</label>
                    <input type="text" name="phone" value="{{$user->profile->phone}}" class="form-control" placeholder="Телефон">
                </div>
                <div class="col-md-4">
                    <label>Дата рождения</label>
                    <input type="text" name="birth_date" value="{{$user->profile->birth_date}}" class="form-control" placeholder="Дата рождения">
                </div>
            </div>
            @endif
        </div>
        <br>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary">
                    Изменить
                </button>
            </div>
            <div class="col-md-4"></div>
        </div>
        <br>
    </form>
    @push('scripts')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @endpush
@endsection
