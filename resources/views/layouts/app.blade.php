<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Сайт') }}</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .cst-head {
            text-transform: uppercase;
        }

        p {
            margin: 0px;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>
<body>
<div class="wrapper">

    @guest
        <div class="container">
            <header class="blog-header py-3">
                <div class="row flex-nowrap justify-content-between align-items-center">
                    <div class="col-12 text-center">
                        <a class="blog-header-logo text-dark" href="#">Автоматизированная система сбора и обработки информации о
                            планируемых научных мероприятиях</a>
                    </div>
                </div>
            </header>
        </div>
    @endguest

    @auth
        <div class="container">
            <header class="blog-header py-3">
                <div class="row flex-nowrap justify-content-between align-items-center">
                    <div class="col-6">
                        @yield('navigation')
                    </div>
                    <div class="col-6 text-right">
                        <a class="blog-header-logo text-dark" href="#">{{Auth::user()->profile->first_name}} {{Auth::user()->profile->last_name}}</a>
                        <a class="blog-header-logo text-dark" href="{{route('logout')}}"><i class="fas fa-power-off icon-red"></i></a>
                    </div>
                </div>
            </header>
        </div>
    @endauth

    <main role="main" class="container blog-main cst-content">
        @yield('content')
    </main>

    <footer class="blog-footer">
        <p>2019 ФГБОУ ВО "Астраханский государственный университет"</p>
    </footer>
</div>

<!-- Scripts -->
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
@stack('scripts')

</body>
</html>
