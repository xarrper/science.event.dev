<?php

use Illuminate\Database\Seeder;

class SourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sources')->insert([[
            'id' => 1,
            'function' => 'rsci',
            'url' => 'http://rsci.ru',
        ], [
            'id' => 2,
            'function' => 'iask',
            'url' => 'https://iask.hu/en/',
        ], [
            'id' => 3,
            'function' => 'flf',
            'url' => 'https://flf-russia.com/',
        ], [
            'id' => 4,
            'function' => 'ascon',
            'url' => 'http://edu.ascon.ru/main/news/',
        ], [
            'id' => 5,
            'function' => 'infra',
            'url' => 'https://infra-m.ru/',
        ]]);
    }
}
