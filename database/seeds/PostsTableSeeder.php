<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([[
            'id' => 1,
            'title' => 'бакалавр',
            'role_id' => 4,
        ], [
            'id' => 2,
            'title' => 'специалист',
            'role_id' => 4,
        ],[
            'id' => 3,
            'title' => 'магистр',
            'role_id' => 4,
        ],[
            'id' => 4,
            'title' => 'старший преподаватель',
            'role_id' => 3,
        ],[
            'id' => 5,
            'title' => 'младший научный сотрудник',
            'role_id' => 3,
        ],[
            'id' => 6,
            'title' => 'специалист',
            'role_id' => 2,
        ]]);
    }
}
