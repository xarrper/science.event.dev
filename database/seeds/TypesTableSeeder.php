<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([[
            'id' => 1,
            'title' => 'грант',
        ], [
            'id' => 2,
            'title' => 'конкурс',
        ], [
            'id' => 3,
            'title' => 'конференция',
        ]]);
    }
}
