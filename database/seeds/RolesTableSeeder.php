<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'id' => 1,
            'title' => 'admin',
        ], [
            'id' => 2,
            'title' => 'spec',
        ], [
            'id' => 3,
            'title' => 'teacher',
        ], [
            'id' => 4,
            'title' => 'student',
        ]]);
    }
}
