<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeyToMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('measures', function (Blueprint $table) {
            $table->dropForeign('measures_type_id_foreign');
            $table->foreign('type_id')
                ->references('id')->on('types')
                ->onDelete('cascade')
                ->change();

            $table->dropForeign('measures_source_id_foreign');
            $table->foreign('source_id')
                ->references('id')->on('sources')
                ->onDelete('cascade')
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('measures', function (Blueprint $table) {
            //
        });
    }
}
