$( document ).ready(function() {

    function calendar() {
        $('input[name="birth_date"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сетябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1,
            },
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10), function(start, end, label) {
                var years = moment().diff(start, 'years');
                alert("You are " + years + " years old!");
            }
        });
    }

    function date(name) {
        let datepicker = $('input[name=' + name + ']');
        datepicker.daterangepicker({
            singleDatePicker: true,
            autoApply: false,
            autoUpdateInput: false,
            showDropdowns: true,
            locale: {
                cancelLabel: 'Очистить',
                applyLabel: 'Применить',
                format: 'YYYY-MM-DD',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сетябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1,
            }
        });
        datepicker.on('show.daterangepicker', () => {
            jQuery('.daterangepicker').removeClass('auto-apply');
        })
        datepicker.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        datepicker.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });
    }

    if($("#page-register").length) {

        optionVisible();

        function optionVisible(flag = false) {

            let radioValue = $("input[name='role_id']:checked").val();

            $("#select-post > option").each(function () {
                if ($(this).data('role') == radioValue) {
                    $(this).removeClass('el-none');
                } else {
                    $(this).addClass('el-none');
                }
                if ($(this).data('role')) {
                    $(this).removeAttr('selected');
                }
            });

            if(radioValue == 2) {
                $('.field-user').addClass('el-none');
            } else {
                $('.field-user').removeClass('el-none');
            }

            if(flag) {
                $("#select-post option:first").attr('selected', 'selected');
            }
        }

        $('input[type=radio][name=role_id]').change(function () {
            optionVisible(true);
        });

        calendar();

    }

    if($("#profile").length) {
        calendar();
    }

    if($("#measures").length) {
        date('start_date');
        date('end_date');
    }

});
