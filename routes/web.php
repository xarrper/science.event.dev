<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/register', 'Auth\RegisterController@index')->name('register');

Route::get('/admin', 'HomeController@userList')->name('user.list')->middleware('auth', 'role:admin');

Route::post('/userActive/{user}', 'HomeController@userActive')->name('user.active')->middleware('auth', 'role:admin');

Route::get('/logout', 'HomeController@logout')->name('logout');


Route::get('/user/cabinet', 'UserController@index')->name('user.cabinet')->middleware('auth', 'role:student,teacher');
Route::get('/user/cabinet/profile', 'SpecController@profile')->name('user.profile')->middleware('auth', 'role:student,teacher');
Route::get('/user/cabinet/measure', 'UserController@listMeasure')->name('user.cabinet.measure')->middleware('auth', 'role:student,teacher');
Route::post('/user/toggle/favorite/{measure}', 'UserController@toggleFavorite')->name('user.toggle.favorite')->middleware('auth', 'role:student,teacher');
Route::get('/user/cabinet/measure/favorite', 'UserController@favoriteMeasure')->name('user.cabinet.measure.favorite')->middleware('auth', 'role:student,teacher');
Route::get('/user/cabinet/measure/offer', 'UserController@offerMeasure')->name('user.cabinet.measure.offer')->middleware('auth', 'role:student,teacher');

Route::get('/spec/cabinet', 'SpecController@index')->name('spec.cabinet')->middleware('auth', 'role:spec');
Route::get('/spec/cabinet/users', 'SpecController@listUsers')->name('spec.cabinet.users')->middleware('auth', 'role:spec');
Route::get('/spec/cabinet/sources', 'SpecController@sources')->name('spec.sources')->middleware('auth', 'role:spec');
Route::post('/spec/remove/sources/{source}', 'SpecController@removeSources')->name('spec.remove.sources')->middleware('auth', 'role:spec');
Route::post('/spec/save/sources', 'SpecController@saveSources')->name('spec.save.sources')->middleware('auth', 'role:spec');
Route::get('/spec/cabinet/profile', 'SpecController@profile')->name('spec.profile')->middleware('auth', 'role:spec');
Route::post('/spec/cabinet/profile/update/{user}', 'SpecController@profileUpdate')->name('spec.profile.update')->middleware('auth');
Route::get('/spec/cabinet/measure', 'MeasureController@index')->name('spec.cabinet.measure')->middleware('auth', 'role:spec');
Route::post('/parser', 'MeasureController@parser')->name('parser')->middleware('auth', 'role:spec');
Route::post('/spec/remove/measures/{measure}', 'MeasureController@removeMeasures')->name('spec.remove.measures')->middleware('auth', 'role:spec');
Route::post('/spec/save/measure', 'MeasureController@saveMeasures')->name('spec.save.measures')->middleware('auth', 'role:spec');
Route::get('/spec/edit/measure/{id}', 'MeasureController@editMeasures')->name('spec.edit.measures')->middleware('auth', 'role:spec');
Route::post('/spec/update/measure/{measure}', 'MeasureController@updateMeasures')->name('spec.update.measures')->middleware('auth', 'role:spec');
Route::get('/spec/create/measure', 'MeasureController@createMeasures')->name('spec.create.measures')->middleware('auth', 'role:spec');


Route::get('/test', 'MeasureController@parser');
