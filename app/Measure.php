<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Measure extends Model
{

    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'users_measures', 'measure_id', 'user_id');
    }

    public function favorite()
    {
        $count = $this->user->filter(function($item) {
            return $item->id == Auth::user()->id;
        })->count();

        return ($count) ? true : false;
    }

    public function offer()
    {
        return $this->user->filter(function($item) {
            return $item->id == Auth::user()->id;
        })->count();
    }

}
