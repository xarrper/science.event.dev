<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function filterArrayByExistKey($array, $existKeys)
    {
        return array_filter($array, function ($key) use ($existKeys) {
            return in_array($key, $existKeys);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function sanitize()
    {
        $existFields = ['url', 'function'];

        $input = $this->filterArrayByExistKey($this->all(), $existFields);

        $this->replace($input);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'url' => 'required',
            'function' => 'required',
        ];
    }
}
