<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeasureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function filterArrayByExistKey($array, $existKeys)
    {
        return array_filter($array, function ($key) use ($existKeys) {
            return in_array($key, $existKeys);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function sanitize()
    {
        $existFields = ['text', 'type_id', 'link', 'start_date', 'end_date', 'condition'];

        $input = $this->filterArrayByExistKey($this->all(), $existFields);

        $this->replace($input);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'text' => 'required',
            'type_id' => 'required',
            'link' => 'required',
        ];
    }
}
