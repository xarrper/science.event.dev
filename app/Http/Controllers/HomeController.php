<?php

namespace App\Http\Controllers;

use App\User;
use Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->hasRole('admin')) {
            return redirect()->route('user.list');
        }
        elseif(Auth::user()->hasRole('spec')) {
            return redirect()->route('spec.cabinet');
        }
        elseif(Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher')) {
            return redirect()->route('user.cabinet');
        }
        return view('home');
    }

    public function userList()
    {
        $users = User::with('profile')->whereNotIn('role_id', [1])->get();
        return view('user.list', ['users' => $users]);
    }

    public function userActive(User $user)
    {
        $user->confirmed = ($user->confirmed) ? 0 : 1;
        $user->save();
        return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->back();
    }

}
