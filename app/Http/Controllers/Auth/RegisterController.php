<?php

namespace App\Http\Controllers\Auth;

use App\Post;
use App\Profile;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'last_name' => ['required', 'alpha'],
            'first_name' => ['required', 'alpha'],
            'middle_name' => ['required', 'alpha'],
            'post_id' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'email' => $data['email'],
            'confirmed' => 0,
            'role_id' => $data['role_id'],
            'password' => Hash::make($data['password']),
        ]);

        if($data['role_id'] == 2) {
            $data['phone'] = null;
            $data['group'] = null;
            $data['degree'] = null;
            $data['birth_date'] = null;
        }

        Profile::create([
            'user_id' => $user->id,
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'degree' => $data['degree'],
            'group' => $data['group'],
            'phone' => $data['phone'],
            'post_id' => $data['post_id'],
            'birth_date' => $data['birth_date'],
        ]);

        return $user;
    }

    public function index()
    {
        $posts = Post::all();
        return view('auth.register', [
            'posts' => $posts,
        ]);
    }

    protected function registered(Request $request, $user)
    {
        Auth::logout();
    }
}
