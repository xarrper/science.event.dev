<?php

namespace App\Http\Controllers;

use App\Measure;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class UserController extends Controller
{

    private $model;

    public function __construct(Measure $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        return view('user.cabinet');
    }

    public function listMeasure(Request $request, Type $type)
    {
        $measures = $this->model;

        if(!is_null($request->type_id)) {
            $measures = $measures->where('type_id', $request->type_id);
        }
        if(!is_null($request->start_date)) {
            $measures = $measures->where('start_date', '>=', $request->start_date);
        }
        if(!is_null($request->end_date)) {
            $measures = $measures->where('end_date', '<=', $request->end_date);
        }

        $measures = $measures->get();

        $measures = $measures->diff(Auth::user()->measures);
        return view('user.measures', ['measures' => $measures, 'types' => $type->all()]);
    }

    public function toggleFavorite(Measure $measure)
    {
        if($measure->favorite()) {
            DB::table('users_measures')->where([['user_id', '=', Auth::user()->id],['measure_id', '=', $measure->id]])->delete();
        } else {
            DB::table('users_measures')->insert(
                ['measure_id' => $measure->id, 'user_id' => Auth::user()->id]
            );
        }
        return back();
    }

    public function favoriteMeasure()
    {
        $measures = Auth::user()->measures;
        return view('user.favorite-measures', ['measures' => $measures]);
    }

    public function offerMeasure()
    {
        $users = DB::select('SELECT users.id FROM users, profiles WHERE profiles.post_id = (SELECT post_id FROM profiles WHERE profiles.user_id = '.Auth::user()->id.') and profiles.user_id = users.id');

        $users = array_map(function($item) {
            return $item->id;
        }, $users);

        $users = array_filter($users, function($item) {
            return $item !== Auth::user()->id;
        });
        $str_users = implode(",", $users);

        $measuresId = DB::select('SELECT DISTINCT measures.id FROM measures, users_measures WHERE  users_measures.measure_id = measures.id and users_measures.user_id in ('.$str_users.')');
        $measuresId = array_map(function($item) {
            return $item->id;
        }, $measuresId);

        $measures = $this->model
            ->whereIn('id', $measuresId)
            ->get();

        return view('user.offer-measures', ['measures' => $measures]);
    }

}
