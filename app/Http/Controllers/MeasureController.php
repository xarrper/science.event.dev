<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeasureRequest;
use App\Measure;
use App\Source;
use App\Type;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Http\Request;
use Exception;

class MeasureController extends Controller
{
    private $model;

    public function __construct(Measure $model)
    {
        $this->model = $model;
    }

    public function index(Request $request, Type $type) {
        $measures = $this->model;
        if(!is_null($request->type_id)) {
            $measures = $measures->where('type_id', $request->type_id);
        }
        if(!is_null($request->start_date)) {
            $measures = $measures->where('start_date', '>=', $request->start_date);
        }
        if(!is_null($request->end_date)) {
            $measures = $measures->where('end_date', '<=', $request->end_date);
        }
        return view('spec.measures', ['measures' => $measures->get(), 'types' => $type->all()]);
    }

    public function parser(Source $model) {
        $sources = $model->get();
//        throw new \Exception('ошибка');
        foreach ($sources as $source) {
            $function = $source->function;
            if (method_exists($this, $function)) {
                $this->$function($source->id);
            }
        }
        return response('success', 200);
    }

    public function removeMeasures(Measure $measure)
    {
        $measure->delete();
        return back();
    }

    private function rsci($source_id) {
        $type_id = Type::GRANT;
        $url = 'http://rsci.ru';

        $this->model->where('source_id', $source_id)->delete();

//        $html = file_get_contents('http://rsci.ru/grants/grant_news/');
//        $document = HtmlDomParser::str_get_html($html);
//        $lastPage = $document->find('.pagination ul', 0)->last_child()->find('a',0)->plaintext;

        $lastPage = 10;

        for($i = 1; $i<=(int)$lastPage; $i++) {
            try {
                $html = file_get_contents('http://rsci.ru/grants/grant_news/?PAGEN_1=' . $i);
                $document = HtmlDomParser::str_get_html($html);
                $grands = $document->find('.info-card');
                $measures = [];
                foreach ($grands as $grand) {
                    try {
                        $text = $grand->find('.text-title', 0)->plaintext;
                        $link = $url . $grand->find('a', 0)->href;
                        $time = $document->find('time', 0);
                        $dateTime = explode(".", trim($time->first_child()->plaintext));
                        $startDate = trim($time->last_child()->plaintext) . '-' . $dateTime[1] . '-' . $dateTime[0];
                        $measures[] = [
                            'text' => $text,
                            'link' => $link,
                            'start_date' => $startDate,
                            'type_id' => $type_id,
                            'source_id' => $source_id
                        ];
                    }
                    catch (Exception $e) {}
                }
            }
            catch (Exception $e) {}
            try {
                $this->model->insert($measures);
            }
            catch (Exception $e) {}
        }
    }

    private function itmp($source_id) {
        $url = 'https://itmp.msu.ru';
        $type_id = Type::CONTEST;

        $this->model->where('source_id', $source_id)->delete();

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $html = file_get_contents("https://itmp.msu.ru/calls/", false, stream_context_create($arrContextOptions));


        $document = HtmlDomParser::str_get_html($html);
        $measures = [];
        $newsItems = $document->find('.stdpage-contents p span');
        foreach ($newsItems as $newsItem) {
            try {
                $text = trim($newsItem->find('a', 0)->plaintext);
                $link = $url . trim($newsItem->find('a', 0)->href);
                $measures[] = [
                    'text' => $text,
                    'link' => $link,
                    'type_id' => $type_id,
                    'source_id' => $source_id
                ];
            } catch (Exception $e) {

            }

        }
        try {
            $measures = array_unique($measures, SORT_REGULAR);
            $this->model->insert($measures);
        }
        catch (Exception $e) {}
    }

    private function jsString($str='') {
        return preg_replace("/('|\"|\r?\n)/", '', $str);
    }

    private function fpi($source_id) {

        $url = 'https://fpi.gov.ru';
        $type_id = Type::CONTEST;

        $this->model->where('source_id', $source_id)->delete();

        $html = file_get_contents('https://fpi.gov.ru/tenders/');
        $document = HtmlDomParser::str_get_html($html);
        $items = $document->find('.con-item');
        $measures = [];
        foreach ($items as $item) {
            try {
                $text = $item->find('.con-text .ct-hdr', 0)->plaintext . $this->jsString(trim($item->find('.con-text .ct-p', 0)->plaintext));
                $link = $url . $item->href;
                $measures[] = [
                    'text' => $text,
                    'link' => $link,
                    'type_id' => $type_id,
                    'source_id' => $source_id
                ];
            } catch (Exception $e) {}
        }
        try {
            $this->model->insert($measures);
        }
        catch (Exception $e) {}
    }

    private function rscf($source_id) {
        $url = 'http://rscf.ru';
        $type_id = Type::CONTEST;

        $this->model->where('source_id', $source_id)->delete();

        $html = file_get_contents('http://rscf.ru/ru/contests/');
        $document = HtmlDomParser::str_get_html($html);
        $items = $document->find('.table-striped tr');
        $measures = [];

        foreach ($items as $item) {
            try {
                $text = trim($item->find('p', 0)->plaintext);
                $link = $url . $item->find('a', 0)->href;

                $date = trim($item->find('.views-field-field-app-date', 0)->plaintext);
                $pattern = "/\d{2}\.\d{2}\.\d{4}/";
                preg_match($pattern, $date, $matches);
                $startDate = explode('.', $matches[0]);

                $measures[] = [
                    'text' => $text,
                    'link' => $link,
                    'type_id' => $type_id,
                    'start_date' => $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0],
                    'source_id' => $source_id
                ];
            } catch (Exception $e) {}
        }
        try {
            $this->model->insert($measures);
        }
        catch (Exception $e) {}
    }

    private function rfbr($source_id) {
        $url = 'https://www.rfbr.ru';
        $type_id = Type::CONTEST;

        $this->model->where('source_id', $source_id)->delete();

        $html = file_get_contents('https://www.rfbr.ru/rffi/ru/contest/n_812?page=1');
        $document = HtmlDomParser::str_get_html($html);
        $items = $document->find('.table tbody .tr');

        $measures = [];
        foreach ($items as $item) {
            try {
                $text = trim($item->find('.link', 0)->plaintext);
                $link = $url . $item->find('.link', 0)->href;
                $date = $item->find('td', 4)->plaintext;
                $pattern = "/\d{2}\.\d{2}\.\d{4}/";
                preg_match($pattern, $date, $matches);
                $endDate = explode('.', $matches[0]);
                $measures[] = [
                    'text' => $text,
                    'link' => $link,
                    'end_date' => $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0],
                    'type_id' => $type_id,
                    'source_id' => $source_id
                ];
            } catch (Exception $e) {}
        }
        try {
            $this->model->insert($measures);
        }
        catch (Exception $e) {}
    }

    public function saveMeasures(MeasureRequest $request)
    {
        $this->model->fill($request->all());
        $this->model->save();
        return back();
    }

    public function editMeasures(Request $request, Type $type)
    {
        $measure = $this->model->find($request->id);
        return view('spec.edit-measure', ['measure' => $measure, 'types' => $type->all()]);
    }

    public function updateMeasures(MeasureRequest $request, Measure $measure)
    {
        $data = [
            "text" => $request->text,
            "type_id" => $request->type_id,
            "link" => $request->link,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "condition" => $request->condition,
        ];

        $measure->update($data);
        $measure->save();
        return back();
    }

    public function createMeasures(Type $type)
    {
        return view('spec.create-measure', ['types' => $type->all()]);
    }

}
