<?php

namespace App\Http\Controllers;

use App\Http\Requests\SourceRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Post;
use App\Source;
use App\User;
use Auth;

class SpecController extends Controller
{
    public function index()
    {
        return view('spec.cabinet');
    }

    public function listUsers()
    {
        $users = User::with('profile')->whereNotIn('role_id', [1, 2])->get();
        return view('spec.userList', ['users' => $users]);
    }

    public function sources()
    {
        $sources = Source::all();
        return view('spec.sources', ['sources' => $sources]);
    }

    public function removeSources(Source $source)
    {
        $source->delete();
        return back();
    }

    public function saveSources(SourceRequest $request, Source $source)
    {
        $source->fill($request->all());
        $source->save();
        return back();
    }

    public function profile()
    {
        $user = Auth::user();
        $posts = Post::where('role_id', $user->role_id)->get();
        return view('user.profile', ['user' => $user, 'posts' => $posts]);
    }

    public function profileUpdate(UserUpdateRequest $request, User $user)
    {

        $user->email = $request->email;

        $profile = [
            'user_id' => $user->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'post_id' => $request->post_id,
            'phone' => $request->phone,
            'group' => $request->group,
            'degree' => $request->degree,
            'birth_date' => $request->birth_date
        ];

        $user->profile()->update($profile);
        $user->save();
        return back();
    }

}
