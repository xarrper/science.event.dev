<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public const GRANT = 1;
    public const CONTEST = 2;
}
