<?php

namespace App\Helpers;

use Date;

class AppHelper
{
    public static function checkRelevance($start, $end)
    {
        $now = date('Y-m-d');
        if($start && $end) {
            return (($start <= $now) && ($now <= $end));
        }
        elseif($start && !$end) {
            return $start == $now;
        } elseif(!$start && $end) {
            return $end == $now;
        }
        return false;
    }
}
